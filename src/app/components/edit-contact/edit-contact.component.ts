import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IContact } from 'src/app/models/IContact';
import { IGroup } from 'src/app/models/IGroup';
import { ContactService } from 'src/app/services/contact.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-contact',
  templateUrl: './edit-contact.component.html',
  styleUrls: ['./edit-contact.component.css']
})
export class EditContactComponent implements OnInit {

 

  public loading: boolean = false;
  public contactId: any;
  public groups: any;
  public contact:IContact = {} as IContact;
  public errorMessage:string | null = null;

  constructor(private activatedRoute: ActivatedRoute, private contactService: ContactService, private router: Router) {

   }

  ngOnInit(): void {

    this.activatedRoute.paramMap.subscribe((param) => {
        this.contactId = param.get('contactId');
    });
    if (this.contactId){
      this.contactService.getContact(this.contactId).subscribe((data) =>{
            this.contact = data;
            this.loading = false;
            this.contactService.getAllContacts().subscribe((data) =>{
              this.groups = data;
        })

      }, (error) => {
          this.errorMessage = error;
          this.loading = false;

      })
    }
    
  }

  updateContact() {
    if (this.contactId){
    this.contactService.updateContact(this.contact, this.contactId).subscribe(() => {
  
      this.router.navigate(['/']).then();
  
   }, (error) => {
  
      this.errorMessage = error;
      this.router.navigate([`/contacts/edit/${this.contactId}`]).then(); 
    });
  } 

}
}
